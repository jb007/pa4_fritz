Content:   Programming Assignment 3
Authors:   Margareta Ciglic, Friedrich Knauder, Johannes Zlattinger
GroupId:   Group01


Directory MyFD
	The directory MyFD has the following subdir structure:
		+- MyFD
		   +- export
		   +- lib
		   +- src

	The directory export contains an excel file experiments.xls which holds the actual results and histograms of the executed experiments.



File Report.pdf
	This file describes project and experiments.
		

Execution Instructions
    The project contains an ant build-file. The most important targets are as follows.

    ant test - Runs basic tests and the required experiments in one single process on one single machine.

    ant package - Creates a single jar file containing everything which is needed to run a peer. The resulting jar-file can then be run to start the
                  initial peer using a plain command line 'java -jar peer.jar -p {SERVER_PORT}'. SERVER_PORT specifies which port the server should
                  listen to for incoming connections. To start another peer connecting to the first one a new process must be started
                  'java -jar peer.jar -A {IP} -P {PORT} -p {SERVER_PORT}'. IP and PORT specifies ip and port of the peer to connect to.

                  e.g. Initial Peer: 'java -jar peer.jar -p 2221'
                       Second Peer: 'java -jar peer.jar -A 127.0.0.1 -P 2221 -p 2222'
