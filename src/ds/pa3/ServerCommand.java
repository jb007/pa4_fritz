package ds.pa3;

import com.esotericsoftware.kryonet.Connection;

/**
 * A <i>ServerCommand</i> is a {@link Command} which is executed by the server.
 * A client prepares the necessary parameters sends it to the server who then performs the
 * corresponding action.
 */
public abstract class ServerCommand extends Command {

    protected void sendResponse(Connection connection, Peer peer, Command cmd) {
        cmd.serverPort = peer.serverPort;
        connection.sendTCP(cmd);
    }
}
