package ds.pa3;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.util.*;

class Peer {

    public static final int DEFAULT_PORT = 54001;

    public static boolean systemOutWithPort = true;
    
    public static Options options = new Options();

    static {
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .create('A'));
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .create('P'));
        options.addOption(OptionBuilder.withLongOpt("port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .create('p'));
    }

    public static void main(String[] args) throws Exception {
        CommandLine cmd = new PosixParser().parse(options, args);

        Peer peer = new Peer(cmd.hasOption("port") ? ((Number) cmd.getParsedOptionValue("port")).intValue() : DEFAULT_PORT);

        if (cmd.hasOption("remote-address")) {
            connectToInitialPeer(peer,
                                 cmd.getOptionValue("remote-address"),
                                 cmd.hasOption("remote-port") ? ((Number) cmd.getParsedOptionValue("remote-port")).intValue() : DEFAULT_PORT);
        }

        System.out.println("--> Ready (press Ctrl+C to quit)");
    }

    public static void connectToInitialPeer(Peer peer, String remoteHost, int remotePort) {
        try {
            RemotePeer remotePeer = new RemotePeer(remoteHost, remotePort, peer, peer.getMessageStats());
            remotePeer.requestJoin();
            peer.addRemotePeer(remotePeer);
        } catch (IOException e) {
            System.out.println(">>> ERROR --> Could not connect to peer " + remoteHost + ":" + remotePort);
        }
    }

    /**
     * A <i>ServerListener</i> is responsible to handle incoming commands (see {@link ServerCommand}) and perform
     * the corresponding action.
     */
    private class ServerListener extends Listener {

        private Peer peer;

        public ServerListener(Peer peer) {
            this.peer = peer;
        }

        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof ServerCommand) {
                ServerCommand cmd = (ServerCommand) object;
                String remotePeerAddress = connection.getRemoteAddressTCP().getAddress().getHostAddress() + ":" + cmd.serverPort;

                stats.incReceivedCount();

                log("Received " + cmd.getName() + " from " + remotePeerAddress);

                cmd.perform(connection, peer, remotePeers.get(remotePeerAddress));

                if (remotePeers.get(remotePeerAddress) != null) remotePeers.get(remotePeerAddress).renewTimeoutTimer();
            } else if (!(object instanceof FrameworkMessage)) {
                throw new RuntimeException("Received unknown message of type " + object.getClass().toString());
            }
        }
    }

    public static final int TIMEOUT = 10000;

    final Map<String, RemotePeer> remotePeers = Collections.synchronizedMap(new HashMap<String, RemotePeer>());

    final int serverPort;

    final Server server;

    final MessageStats stats = new MessageStats();

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Peer Construction & Initialization
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public Peer(int port) throws IOException {
        log("--> Starting up " + InetAddress.getLocalHost().getHostAddress() + ":" + port);

        serverPort = port;

        server = prepareServer();
        server.addListener(new ServerListener(this));
        server.start();
        server.bind(port);

        installShutdownHook();
    }

    private Server prepareServer() {
        Server server = new Server();
        server.getKryo().register(String[].class);
        server.getKryo().register(JoinRequest.class);
        server.getKryo().register(JoinResponse.class);
        server.getKryo().register(Ping.class);
        return server;
    }

    /**
     * Installing signal handler for graceful shutdown.
     * The strange try-catch construct is necessary to be able to at least try to close all
     * connections. A single try-catch-block would prevent that.
     */
    private void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });
    }

    public void shutdown() {
        log("Shutting down ...");

        for (RemotePeer peer : remotePeers.values()) {
            try {
                peer.closeConnection();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        try {
            server.stop();
        } catch (Exception e) {
            System.err.println(e);
        }
        printMessageStats();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Remote Peer Management
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public Map<String, RemotePeer> getRemotePeers() {
        return remotePeers;
    }

    public synchronized void addRemotePeer(final RemotePeer remotePeer) {
        remotePeers.put(remotePeer.getAddress(), remotePeer);
    }

    public synchronized void removeRemotePeer(final RemotePeer remotePeer) {
        remotePeers.remove(remotePeer.getAddress());
    }

    public synchronized boolean containsRemotePeer(RemotePeer remotePeer) {
        return remotePeers.containsKey(remotePeer.getAddress());
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Message Statistics
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public MessageStats getMessageStats() {
        return stats;
    }

    public void printMessageStats() {
        synchronized (this.getClass()) {
            IOStream.getMsgStatPrintStream().println("");
            if (systemOutWithPort) {
                System.out.println("===== Message Statistics (" + this.serverPort + ") =====");
            } else {
                System.out.println("===== Message Statistics =====");
            }
            IOStream.getMsgStatPrintStream().println("Number of sent messages: " + stats.getSentCount());
            IOStream.getMsgStatPrintStream().println("Number of received messages: " + stats.getReceivedCount());
            IOStream.getMsgStatPrintStream().println("Number of sent bytes: " + stats.getSentBytesCount());
            IOStream.getMsgStatPrintStream().println("");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Logging
    ///////////////////////////////////////////////////////////////////////////////////////////////
    

    public void log(String message) {
    	
        synchronized (this.getClass()) {
            if (systemOutWithPort) {
                IOStream.getPeerPrintStream().println("--> :" + serverPort + " > " + message);
            } else {
            	IOStream.getPeerPrintStream().println(message);
            }
        }
    }
}