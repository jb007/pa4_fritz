package ds.pa3;

/**
 * A <i>ClientCommand</i> is a {@link Command} which is executed by the client.
 * Typically a <i>ClientCommand</i> is the response to a previously send {@link ServerCommand}.
 * The server prepares the command with the requested information, if any, and sends it to the
 * client who then performs an action to process the response.
 */
public abstract class ClientCommand extends Command {
}
