package ds.pa3;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class RemotePeer {

    /**
     * A <i>ClientListener</i> is responsible to handle server responses (see {@link ClientCommand}) and perform
     * corresponding actions.
     */
    private class ClientListener extends Listener {

        private RemotePeer remotePeer;

        public ClientListener(RemotePeer remotePeer) {
            this.remotePeer = remotePeer;
        }

        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof ClientCommand) {
                ClientCommand cmd = (ClientCommand) object;

                peer.log("Received " + cmd.getName() + " from " + remotePeer.getAddress());

                cmd.perform(connection, peer, remotePeer);
            } else if (!(object instanceof FrameworkMessage)) {
                throw new RuntimeException("Received unkown message of type " + object.getClass().toString());
            }
        }

        @Override
        public void disconnected(Connection connection) {
            remotePeer.closeConnection();
        }
    }

    public static final int TIMEOUT = 10000;

    final Peer peer;

    final Client client;

    final String address;

    final MessageStats stats;

    Timer timeoutTimer;

    public RemotePeer(final String host, final int port, final Peer peer, final MessageStats stats) throws IOException {
        this.address = host + ":" + port;
        this.peer = peer;
        this.stats = stats;

        client = prepareClient();
        client.addListener(new ClientListener(this));
        client.start();
        client.connect(TIMEOUT, host, port);

        renewTimeoutTimer();
    }

    private Client prepareClient() {
        Client client = new Client();
        client.getKryo().register(String[].class);
        client.getKryo().register(JoinRequest.class);
        client.getKryo().register(JoinResponse.class);
        client.getKryo().register(Ping.class);
        return client;
    }

    public String getAddress() {
        return this.address;
    }

    public synchronized void closeConnection() {
        peer.log("Closing connection to " + getAddress());

        client.close();

        timeoutTimer.cancel();
        client.stop();
    }

    public void requestJoin() {
        if (client.isConnected()) {
            sendCommand(new JoinRequest());
        } else {
            final RemotePeer thisRemotePeer = this;
            client.addListener(new Listener() {
                public void connected(Connection connection) {
                    sendCommand(new JoinRequest());
                }
            });
        }
    }

    public void ping() {
        sendCommand(new Ping());
    }

    private synchronized void sendCommand(Command command) {
        if (client.isConnected()) {
            peer.log("Sending " + command.getName() + " to " + address);

            command.serverPort = peer.serverPort;

            stats.incSentBytesBy(client.sendTCP(command));
            stats.incSentCount();

            renewTimeoutTimer();
        } else {
            peer.log("Could not send " + command.getName() + ".");
            peer.log("Removing " + this.getAddress() + " because the connection has been closed!");
            peer.removeRemotePeer(this);
        }
    }

    public synchronized void renewTimeoutTimer() {
        // Ensuring that client is connected
        if (!client.isConnected()) {
            return;
        }

        if (timeoutTimer != null) {
            timeoutTimer.cancel();
        }

        final RemotePeer thisRemotePeer = this;
        timeoutTimer = new Timer();
        timeoutTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                thisRemotePeer.ping();
            }
        }, TIMEOUT);
    }
}
