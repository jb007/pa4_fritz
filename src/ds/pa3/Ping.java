package ds.pa3;

import com.esotericsoftware.kryonet.Connection;

public class Ping extends ServerCommand {
    @Override
    public String getName() {
        return "PING";
    }

    @Override
    public void perform(Connection connection, Peer peer, RemotePeer remotePeer) {
        // Because of TCP it is sufficient to simply receive the command.
        // No further processing is necessary.
    }
}
