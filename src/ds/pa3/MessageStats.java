package ds.pa3;

public class MessageStats {

    private long receivedCount;

    private long sentCount;

    private long sentBytesCount;

    public long getReceivedCount() {
        return receivedCount;
    }

    public synchronized void incReceivedCount() {
        receivedCount += 1;
    }

    public long getSentCount() {
        return sentCount;
    }

    public synchronized void incSentCount() {
        sentCount += 1;
    }

    public long getSentBytesCount() {
        return sentBytesCount;
    }

    public synchronized void incSentBytesBy(int byteCount) {
        sentBytesCount += byteCount;
    }
}
