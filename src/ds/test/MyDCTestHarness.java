package ds.test;

import ds.client.MyDC;
import ds.peer.IOStream;
import ds.peer.Peer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;
import java.util.Vector;

import ds.dc.DCPeer;

public class MyDCTestHarness {

	
    public static Options options = new Options();

    public static ArrayList<DCPeer> peerList = new ArrayList<DCPeer>();
    
    public static int startPort;
    
    // Definition of arguments 
    
    static {
    	// Remote Address of Peer
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .isRequired()
                .create('A'));
        // Remote Port
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .isRequired()
                .create('P'));
        // Number of remote Peers to create
        options.addOption(OptionBuilder.withLongOpt("number-of-peers-to-create")
        		.withArgName("nr_of_peers")
        		.hasArg()
        		.withType(Number.class)
        		.isRequired()
        		.create('n'));
        // Port of Initial Peer
        options.addOption(OptionBuilder.withLongOpt("peer")
        		.withArgName("peer")
        		.hasArg()
        		.withType(Number.class)
        		.isRequired()
        		.create('p'));
    }
    private MyDC myDC;
    private boolean running = true;

    /** 
      * This method creates the peer and its remote peers as specified via arguments 
      */
    public static void main(String[] args)   {
        
    	// Evaluation of Arguments
		CommandLine cmd;
		try {
			cmd = new PosixParser().parse(options, args);
			String host = cmd.getOptionValue("remote-address");
            int port = ((Number) cmd.getParsedOptionValue("remote-port")).intValue();
            int peerPort = ((Number) cmd.getParsedOptionValue("peer")).intValue();
            
            DCPeer peer = null;
			try {
				peer = new DCPeer(peerPort);
			} catch (IOException e) {
				System.err.println("not possible to create peer" + e.toString());
			}
			if (peer != null) {
				peerList.add(peer);   // add created peer to list of available peers
				
				if (cmd.hasOption("remote-address")) {  // are there other peers available?
					DCPeer.connectToInitialPeer(peer, host, port);
				}

	            System.out.println("--> Ready (press Ctrl+C to quit) at " + peer.getAddress());
	                		
	            MyDC myDC = null;
				try {
					int countPeers = (cmd.hasOption("number-of-peers-to-create") ? ((Number) cmd.getParsedOptionValue("number-of-peers-to-create")).intValue() : 0);
					myDC = new MyDC(host, port);
					System.out.println("mydC ready "+ myDC.isReady());
					MyDCTestHarness testHarness = new MyDCTestHarness(myDC); 
					
					// for logging issues export peer outputs to /export/test4.txt
					IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/test4.txt")));  
		            
					// add additional ports as specified
					if (countPeers>0)
						port++;
		            	MyDCTestHarness.creatingDCPeers(port+1, countPeers);
		            MyDCTestHarness.view_DCPeers();
		            MyDCTestHarness.showMenu(myDC);
				} catch (IOException e) {
					System.err.println("not possible to connect to DC" + e.toString());
					e.printStackTrace();
				} catch (Exception e) {
					System.err.println("error occurred " + e.toString());
				}
			}
		} catch (ParseException e) {
			System.err.println("Problems with parsing/arguments  - use correct formating of arguments" + e.toString());
		}
    }
    

    public MyDCTestHarness(MyDC myDC) {
        this.myDC = myDC;
        installShutdownHook();
    }
    
    /**
     * Installing signal handler for graceful shutdown.
     * The strange try-catch construct is necessary to be able to at least try to close all
     * connections. A single try-catch-block would prevent that.
     */
    private void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
            	for (Peer p: peerList) {
            		p.shutdown();
            	}
                myDC.shutdown();
                running = false;
            }
        });
    }

    
    public static void showMenu(MyDC myDC){
		try {
	    	while (true) {
				System.out.println("Menu");	
				System.out.println("[1] -> show current peers");
				System.out.println("[2] -> add peer(s)");
				System.out.println("[3] -> delete peer(s)");
				System.out.println("[4] -> start insert/lookup/delete");
				System.out.println("[5] -> Movie Database insert/delete");
				System.out.println("[6] -> Movie Database lookup");
				System.out.println("[7] -> exit");
				System.out.println(">>:");
				
				Scanner scanner = new Scanner(System.in);
				int selection = scanner.nextInt();
				
				switch (selection){
		             
		           case 1: view_DCPeers();
		                break;
		             
		           case 2:  
	               		System.out.println("enter number of peers to add >>");
		        	    selection = scanner.nextInt();
	               		if (!peerList.isEmpty()) {
	               			String address  = peerList.get(peerList.size()-1).getAddress();
	               			// get last port from List of Peers to create a new Peer 
	               			creatingDCPeers((int)Integer.valueOf(address.substring(address.indexOf(":")+1))+1,(int)Integer.valueOf(selection));	               		
	               			view_DCPeers();
	               		}
						else {
							creatingDCPeers(startPort+100,(int)Integer.valueOf(selection));
							view_DCPeers();
						}
		                break;
		           case 3:
		        	   	if (!peerList.isEmpty()) {
		        		   System.out.println("enter number to peer to delete >>");
		        		   int nrOfPeer = (int)Integer.valueOf(scanner.nextInt());
		        	       System.out.println("shutting down " + peerList.get(nrOfPeer-1).getAddress());
		        	       peerList.get(nrOfPeer-1).shutdown();
		        	       peerList.remove(nrOfPeer-1);
	               		   view_DCPeers();
		               	} else {
						    System.out.println("Peerlist is empty!");
							view_DCPeers();
						}
		        	    break;
		        	    
		           case 4:
		        	   	System.out.println("starting communication with cloud ...");
		        	   	Random random = new Random();
		        	   	System.out.println("mydc ready" + myDC.isReady());
		                Vector<String> keys = new Vector<String>();

		                long insertCounter = 0;
		                long deleteCounter = 0;
		                long lookupCounter = 0;
		                while((insertCounter + deleteCounter + lookupCounter)<100) {
		                    try {
		                        if (myDC.isReady()) {
		                            int op = random.nextInt(5);

		                            if (op == 0 || keys.size() == 0) {
		                                insertCounter += 1;
		                                String key = UUID.randomUUID().toString();
		                                System.out.println(" " + key+ " inserted");
		                                keys.add(key);
		                                myDC.insert(key, key.substring(key.lastIndexOf("-") + 1));
		                            } else if (op == 1) {
		                                deleteCounter += 1;
		                                String key = keys.elementAt(random.nextInt((keys.size())));
		                                keys.remove(key);
		                                myDC.delete(key);

//		                                Thread.sleep(100);

		                                if (myDC.lookup(key) != null) {
		                                    System.out.println("  ## ERROR ## " + key + " still exists");
		                                }
		                            } else {
		                                lookupCounter += 1;
		                                String key = keys.elementAt(random.nextInt((keys.size())));
		                                String value = myDC.lookup(key);
		                                if (!key.substring(key.lastIndexOf("-") + 1).equals(value)) {
		                                    System.out.println("  ## ERROR ## " + key + " -> " + myDC.lookup(key));
		                                }
		                            }
		                        }

		                        if ((insertCounter + deleteCounter + lookupCounter) % 100 == 0) {
		                            System.out.println("#INSERT: " + insertCounter + "   #DELETE: " + deleteCounter + "   #LOOKUP: " + lookupCounter);
		                        }

		                        Thread.sleep(50);
		                    } catch (InterruptedException e) {
		                        ; // Simply ignore exception
		                    }
		                }
		        	   	break;
		           case 5:
		        	   	System.out.println("Adding/Deleting Movie Entries ...");
		        	   	break;
		           case 6:
		        	    System.out.println("Searching in MovieDatabase ...");
		           		
		                break;
		           case 7:
		        	    System.out.println("Exiting ...");
		           		shutdown();
		                System.exit(0);
		                        
		           default:System.out.println("Please enter a valid selection.");
		           
		       };
		    }
		} catch (java.util.InputMismatchException e) { 
			System.out.println("wrong entry");			
			showMenu(myDC);
		} catch (Exception e) {
			System.out.println("error handling ..." +e.toString());
			showMenu(myDC);
		}
		
    }
    
    // This method allows creating n peers with ascending port number from port "startport" 
    public static void creatingDCPeers(int startPort, int n) throws Exception {
    	System.out.println("===> creating " + n + " Peers beginning at port " + startPort + " ..." );
    	for (int i=0;i<n;i++){
    	  peerList.add(new DCPeer(startPort+i));
    	  if (i>0) { 
    		  Peer.connectToInitialPeer(peerList.get(i), "127.0.0.1", startPort); 
    	  }
        }
    }
    // method needed to release ports
    public static void shutdown() {
    	for (DCPeer dcp : peerList) {
    		dcp.shutdown();
    	}
    }
    
    // Show all available peers registered at pc
    public static void view_DCPeers() {
    	if (!peerList.isEmpty()) {
    		System.out.println(" ====== available peers: =====");
    		int i = 1;
    		for (DCPeer dcp : peerList) {
    			System.out.println(i + ") " +   dcp.getAddress());
    			i++;
    		}
    	} else {
    		System.out.println("peer list is empty!");
    	}
    		
    }
    
    // Log Management 
    public void log(String message) {
    	synchronized (this.getClass()) {
    		IOStream.getTestPrintStream().println(message);
    	}
    }
}
