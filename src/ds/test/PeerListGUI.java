package ds.test;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import ds.client.MyDC;
import ds.peer.Peer;
import ds.peer.RemotePeer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;
import java.awt.BorderLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
 
import javax.script.ScriptException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import ds.dc.DCPeer;




public class PeerListGUI extends JPanel
                      implements ListSelectionListener {

    public static Options options = new Options();

    static ArrayList<DCPeer> peerList = new ArrayList<DCPeer>();
    private static Integer tfArgNewPeers;
    private static int startPort;
    
    
    static {
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .isRequired()
                .create('A'));
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .isRequired()
                .create('P'));
    }

	public JList list;
    public DefaultListModel listModel;

    private static final String addString = "Add Peer";
    private static final String deleteString = "Delete Peer";
    private JButton delButton;
    public  JTextField tfPeerCount;
    private Map<String, RemotePeer> remotePeers = Collections.synchronizedMap(new HashMap<String, RemotePeer>());

    public PeerListGUI(MyDC myDC, String host, String port) {
        super(new BorderLayout());

        this.myDC = myDC;
        
        
        listModel = new DefaultListModel();
        
        listModel.addElement(host + ":" +  port);

        //Create the list and put it in a scroll pane.
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);

        JButton addButton = new JButton(addString);
        AddPeerListener addListener = new AddPeerListener(addButton);
        addButton.setActionCommand(addString);
        addButton.addActionListener(addListener);
        addButton.setEnabled(false);

        delButton = new JButton(deleteString);
        delButton.setActionCommand(deleteString);
        delButton.addActionListener(new DelPeerListener());

        tfPeerCount = new JTextField(5);
        tfPeerCount.addActionListener(addListener);
        tfPeerCount.getDocument().addDocumentListener(addListener);
        String name = listModel.getElementAt(
                              list.getSelectedIndex()).toString();

        JLabel statusLabel = new JLabel();
        statusLabel.setText("ready");
        
        //Create a panel that uses BoxLayout.
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel,
                                           BoxLayout.LINE_AXIS));
        buttonPanel.add(delButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(tfPeerCount);
        buttonPanel.add(addButton);
        
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        add(listScrollPane, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.PAGE_END);
    }

    class DelPeerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //This method can be called only if
            //there's a valid selection
            //so go ahead and remove whatever's selected.
            int index = list.getSelectedIndex();
            listModel.remove(index);

            int size = listModel.getSize();

            if (size == 0) { //Nobody's left, disable deleteButton.
                delButton.setEnabled(false);

            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }

                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }

    //This listener is shared by the text field and the hire button.
    class AddPeerListener implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;

        public AddPeerListener(JButton button) {
            this.button = button;
        }

        //Required by ActionListener.
        public void actionPerformed(ActionEvent e) {
            String count = tfPeerCount.getText();

            //User didn't type a number
            if (count.equals("")) {
                Toolkit.getDefaultToolkit().beep();
                tfPeerCount.requestFocusInWindow();
                tfPeerCount.selectAll();
                return;
            }

            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }
            
                    
            
            listModel.insertElementAt(tfPeerCount.getText(), index);
            

            //Reset the text field.
            tfPeerCount.requestFocusInWindow();
            tfPeerCount.setText("");

            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }

        //This method tests for string equality. You could certainly
        //get more sophisticated about the algorithm.  For example,
        //you might want to ignore white space and capitalization.
        /* protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        } */

        //Required by DocumentListener.
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        //Required by DocumentListener.
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        //Required by DocumentListener.
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }

    //This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {

            if (list.getSelectedIndex() == -1) {
            //No selection, disable del button.
                delButton.setEnabled(false);

            } else {
            //Selection, enable the del button.
                delButton.setEnabled(true);
            }
        }
    }

        
    public static void main(String[] args) {
    	CommandLine cmd;
		try {
			cmd = new PosixParser().parse(options, args);
		    int port;
	        String host = cmd.getOptionValue("remote-address");	
			port = ((Number) cmd.getParsedOptionValue("remote-port")).intValue();
			//Create and set up the window.
	        JFrame frame = new JFrame("Peer Management Group 01");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	        //Create and set up the content pane.
	        JComponent newContentPane;
			try {
				newContentPane = new PeerListGUI(new MyDC(host, port), host, String.valueOf(port));
				 ((PeerListGUI) newContentPane).installShutdownHook();
		        newContentPane.setOpaque(true); //content panes must be opaque
		        frame.setContentPane(newContentPane);
  	            System.out.println("--> Ready (press Ctrl+C to quit)");
  	            frame.setTitle("==> starting test ...");
			    frame.pack();
			    frame.setVisible(true);
  	            ((PeerListGUI) newContentPane).startTest();
			    
			  //Display the window.
			} catch (IOException e) {
				System.out.println("please check if one peer is already started");
				e.printStackTrace();
			}
	       
			
		} catch (ParseException e) {
			System.out.println("Parse error");
			e.printStackTrace();
		}	
    }
    
    /**
     * Installing signal handler for graceful shutdown.
     * The strange try-catch construct is necessary to be able to at least try to close all
     * connections. A single try-catch-block would prevent that.
     */
    private void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
            	for (Peer p: peerList) {
            		p.shutdown();
            	}
                myDC.shutdown();
                running = false;
            }
        });
    }


    private MyDC myDC;

    private boolean running = true;

  

    public void startTest() {
        Random random = new Random();

        Vector<String> keys = new Vector<String>();

        long insertCounter = 0;
        long deleteCounter = 0;
        long lookupCounter = 0;
        while(running) {
            try {
                if (myDC.isReady()) {
                    int op = random.nextInt(5);

                    if (op == 0 || keys.size() == 0) {
                        insertCounter += 1;
                        String key = UUID.randomUUID().toString();
                        System.out.println(" " + key+ " inserted");
                        keys.add(key);
                        myDC.insert(key, key.substring(key.lastIndexOf("-") + 1));
                    } else if (op == 1) {
                        deleteCounter += 1;
                        String key = keys.elementAt(random.nextInt((keys.size())));
                        keys.remove(key);
                        myDC.delete(key);

//                        Thread.sleep(100);

                        if (myDC.lookup(key) != null) {
                            System.out.println("  ## ERROR ## " + key + " still exists");
                        }
                    } else {
                        lookupCounter += 1;
                        String key = keys.elementAt(random.nextInt((keys.size())));
                        String value = myDC.lookup(key);
                        if (!key.substring(key.lastIndexOf("-") + 1).equals(value)) {
                            System.out.println("  ## ERROR ## " + key + " -> " + myDC.lookup(key));
                        }
                    }
                }

                if ((insertCounter + deleteCounter + lookupCounter) % 100 == 0) {
                    System.out.println("#INSERT: " + insertCounter + "   #DELETE: " + deleteCounter + "   #LOOKUP: " + lookupCounter);
                }

                Thread.sleep(50);
            } catch (InterruptedException e) {
                ; // Simply ignore exception
            }
        }
    }
    
}
